const uz = {
  Back_call: "Qayta qo'ng'iroq",
  Home: "Bosh sahifa",
  Category: "Bo'limlar",
  Catalog: "Katalog",
  View_all: "Barcha ko'rsatish",
  Reviews: "Sharhlar",
  About_us: "Biz haqimizda",
  Our_address: "Manzil",
  Contacts: "Kontaktlar",
  Our_social_networks: "Ijtimoiy tarmoqlarimiz",
  Similar_products: "O'xshash mahsulotlar",
  Description: "Tavsif",
  Search: "Qidirish...",
  Request_a_call_back: "Qayta qo'ng'iroq qilishni talab qiling",
  Contact_details: "Kontakt ma'lumotlari",
  Phone_number: "Telefon raqam",
  Filter: "Filtr",
  Brend: "Brend",
  Request: "So'rov Yuborish",
  app_sent: "Sizning so'roviz qabul qilindi, operator 15 minutda sizga qo'ng'iroq qiladi",
  app_error: "Sizning so'rovingiz qabul qilinmadi"
}

export default uz;