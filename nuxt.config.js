import messages from "./locale";


export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Himplast',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
      charset: 'utf-8'
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    },
    {
      hid: 'description',
      name: 'description',
      content: ''
    },
    {
      name: 'format-detection',
      content: 'telephone=no'
    }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/fonts/stylesheet.css',
    "swiper/css/swiper.css",
    '~/assets/styles/main.scss',
    'assets/icomoon/style.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/v-mask.js',
    },
    {
      src: "~/plugins/VueAwesomeSwiper", mode: "client",
    },
    { src: "~/plugins/axios.js", mode: "client" },
    {
      src: "~/plugins/global.js",
      mode: "client"
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https:go.nuxtjs.dev/config-modules
  // modules: [
  //   https://go.nuxtjs.dev/bootstrap
  //   'bootstrap-vue/nuxt',
  // ],
  modules: [
    '@nuxtjs/axios',
    "nuxt-i18n",
  ],

  i18n: {
    locales: [{
      code: "uz",
      name: "O'zb"
    },

    {
      code: "ru",
      name: "Рус"
    },
    {
      code: "en",
      name: "Eng"
    },

    ],
    strategy: "prefix_except_default",
    defaultLocale: "uz",
    vueI18n: {
      fallBackLocale: "uz",
      messages: messages
    }
  },
  axios: {
    baseUrl: 'http://46.36.217.179:5000/api'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
