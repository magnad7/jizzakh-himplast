export const state = () => ({
    listLang: [],
    listProduct: [],
    listCategory: [],
    detailProduct: {}
})

export const getters = {
    listLang: (state) => {
        return state.listLang
    },
    listProduct: (state) => {
        return state.listProduct
    },
    listCategory: (state) => {
        return state.listCategory
    },
    detailProduct: (state) => {
        return state.detailProduct
    },

}

export const mutations = {
    setState(state, { key, payload }) {
        state[key] = payload
    }
}

export const actions = {
    fetchLangList({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get('/lang')
                const { status, data, message } = res
                if (status == 200 || status == 201) {
                    commit('setState', { key: 'listLang', payload: data || [] });
                    resolve({ status, data, })
                } else {
                    reject(message)
                }
            } catch (error) {
                reject(error)
            }
        })
    },
    fetchProductList({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get('/product')
                const { status, data, message } = res
                if (status == 200 || status == 201) {
                    commit('setState', { key: 'listProduct', payload: data || [] });
                    resolve({ status, data, })
                } else {
                    reject(message)
                }
            } catch (error) {
                reject(error)
            }
        })
    },
    fetchDetailProduct({ commit }, { id }) {
        console.log(id);
        return new Promise(async (resolve, reject) => {
            try {
                commit('setState', { key: 'detailProduct', payload: {} });
                const res = await this.$axios.get(`/product/${id}`)
                const { status, data, message } = res
                if (status == 200 || status == 201) {
                    commit('setState', { key: 'detailProduct', payload: data || {} });
                    resolve({ status, data, })
                } else {
                    reject(message)
                }
            } catch (error) {
                reject(error)
            }
        })
    },
    fetchCategoryList({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                commit('setState', { key: 'listCategory', payload: {} });
                const res = await this.$axios.get(`/category`)
                const { status, data, message } = res
                if (status == 200 || status == 201) {
                    commit('setState', { key: 'listCategory', payload: data || {} });
                    resolve({ status, data, })
                } else {
                    reject(message)
                }
            } catch (error) {
                reject(error)
            }
        })
    },
}
