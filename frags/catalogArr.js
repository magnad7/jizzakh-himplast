export default [
  {
    catalogName: {
      en: "CatalogEN 1",
      ru: "КаталогRU 1",
      uz: "KatalogUZ 1",
    },
    catalog_id: 1,
  },
  {
    catalogName: {
      en: "CatalogEN 2",
      ru: "КаталогRU 2",
      uz: "KatalogUZ 2",
    },
    catalog_id: 2,
  },
  {
    catalogName: {
      en: "CatalogEN 3",
      ru: "КаталогRU 3",
      uz: "KatalogUZ 3",
    },
    catalog_id: 3,
  },
]