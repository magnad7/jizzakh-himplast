import Vue from "vue"
export default ({ app }) => {
    Vue.filter("title", function (value) {
        if (value) {
            let locale = app.i18n._vm.locale
            if (value[locale]) {
                return value[locale]
            } else if (value.ru) {
                return value.ru
            }
            return value
        } else return ''
    })
}