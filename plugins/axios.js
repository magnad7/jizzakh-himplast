export default async function ({ $axios, app }) {
    $axios.onRequest(_ => {
        $axios.setHeader("lang", app.i18n.locale || "ru");
    })
}