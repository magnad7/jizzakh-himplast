const en ={
  Back_call: "Back call",
  Category: "Category",
  Catalog: "Catalog",
  View_all: "View all",
  Reviews: "Reviews",
  About_us: "About us",
  Our_address: "Our address",
  Contacts: "Contacts",
  Our_social_networks: "Our social networks",
  Similar_products: "Similar products",
  Description: "Description",
  Search: "Search...",
  Request_a_call_back: "Request a call back",
  Contact_details: "Contact details",
  Phone_number: "Phone number",
  Filter: "Filter",
  Brend: "Brend",
  Request: "Request",
  app_sent: "Your application has been accepted, the operator will call you within 15 minutes",
  app_error: "Your application has not been accepted"
}

export default en;