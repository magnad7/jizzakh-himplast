const ru = {
  Back_call: "Обратный звонок",
  Home: "Главная",
  Category: "Категория",
  Catalog: "Каталог",
  View_all: "Посмотреть все",
  Reviews: "Отзывы",
  About_us: "О нас",
  Our_address: "Наш адресс",
  Contacts: "Контакты",
  Our_social_networks: "Наши Соц. сети",
  Similar_products: "Похожие товары",
  Description: "Описание",
  Search: "Поиск...",
  Request_a_call_back: "Заказать обратный звонок",
  Contact_details: "Контактные данные",
  Phone_number: "Номер телефона",
  Filter: "Фильтр",
  Brend: "Бренд",
  Request: "Заказать",
  app_sent: "Ваш запрос принят, оператор перезвонит вам в течение 15 минут",
  app_error: "Ваш запрос не принят"
}

export default ru;