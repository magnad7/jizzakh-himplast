export default[
  {
    // text: 'Главная',
    text: {
      uz: 'Bosh sahifa',
      ru: 'Главная',
      en: 'Home',
    },
    path: '/'
  },
  {
    // text: 'Каталог',
    text:{
      uz: 'Katalog',
      ru: 'Каталог',
      en: 'Catalog',
    },
    path: '/catalog'
  },
]
